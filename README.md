# kmTimer4Test Application

## Overview

This application, `kmTimer4Test`, is designed to test the [`kmTimer4`](https://gitlab.com/c-mos/kmAvrLibs/kmTimer4) library from the AVR `kmFramework`. It serves also as a demonstration of various functionalities provided by the library.

## Getting sources and compilation
To get the sources, make sure your [gitlab.com](gitlab.com) account is created and correctly configured (e.g. public key defined), then use following git commands to get source code:
```bash
git clone https://gitlab.com/c-mos/kmAvrTests/kmTimer4Test
cd kmTimer4Test
git submodule update --init
git submodule foreach git checkout main
```
Software has been compiled and tested with [**Atmel Studio 7**](http://atmel-studio.s3-website-us-west-2.amazonaws.com/7.0.2397/as-installer-7.0.2397-full.exe)

## Features

- **Timer Configuration**: Demonstrates different configurations of Timer 4.
- **Interrupt Handling**: Illustrates interrupt handling for Timer 4 overflow and compare match events.
- **PWM Generation**: Tests PWM generation capabilities of Timer 4.
- **Debug Output**: Debugging information is provided using the `kmDebug` library.

## Dependencies

This application depends on the following libraries:
- [kmFrameworkAVR](https://gitlab.com/c-mos/kmAvrLibs)
  - [kmCommon](https://gitlab.com/c-mos/kmAvrLibs/kmCommon)
  - [kmCpu](https://gitlab.com/c-mos/kmAvrLibs/kmCpu)
  - [kmDebug](https://gitlab.com/c-mos/kmAvrLibs/kmDebug)
  - [kmTimersCommon](https://gitlab.com/c-mos/kmAvrLibs/kmTimersCommon)
  - [kmTimer4](https://gitlab.com/c-mos/kmAvrLibs/kmTimer4)

Ensure that these libraries are properly included and configured in your project.

## Author and License
**Author**: Krzysztof Moskwa

**e-mail**: chris[dot]moskva[at]gmail[dot]com

**License**: GPL-3.0-or-later

Software License: GNU General Public License (GPL) version 3.0 or later. See [LICENSE.txt](https://www.gnu.org/licenses/gpl-3.0.txt)

 ![GPL3 Logo](https://www.gnu.org/graphics/gplv3-or-later-sm.png)

