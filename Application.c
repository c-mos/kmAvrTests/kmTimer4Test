/*
 * Application.c
 *
 *  Created on: Jan 16, 2024
 *      Author: Krzysztof Moskwa
 *      License: GPL-3.0-or-later
 *
 *  kmTimer4Test Application for testing kmTimer4 library from AVR kmFramework
 *  Copyright (C) 2024  Krzysztof Moskwa
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include <stdio.h>
#include <limits.h>

#include <avr/interrupt.h>
#include <avr/pgmspace.h>
#include <util/delay.h>

#include "kmCpu/kmCpu.h"
#include "kmDebug/kmDebug.h"


#include "kmTimersCommon/kmTimerDefs.h"
#include "kmTimer4/kmTimer4.h"

#define KM_TIMER4_TEST_1MS 1000 // number of microseconds defining 1 millisecond
#define KM_TIMER4_TEST_5MS 5000 // number of microseconds defining 5 millisecond
#define KM_TIMER4_TEST_USER_DATA_A 1UL
#define KM_TIMER4_TEST_USER_DATA_B 65535UL
#define KM_TIMER4_TEST_USER_DATA_C 255UL
#define KM_TIMER4_TEST_USER_DATA_D 4096UL
#define KM_TIMER4_TEST_DUTY_0_PERC KM_TIMER4_BOTTOM
#define KM_TIMER4_TEST_DUTY_25_PERC KM_TIMER4_MID - (KM_TIMER4_MID >> KMC_DIV_BY_2)
#define KM_TIMER4_TEST_DUTY_50_PERC KM_TIMER4_MID
#define KM_TIMER4_TEST_DUTY_75_PERC KM_TIMER4_MID + (KM_TIMER4_MID >> KMC_DIV_BY_2)
#define KM_TIMER4_TEST_DUTY_100_PERC KM_TIMER4_MAX

#define KM_TIMER4_TEST_PHASE_45_DEG KM_TIMER4_TEST_DUTY_25_PERC
#define KM_TIMER4_TEST_PHASE_90_DEG KM_TIMER4_TEST_DUTY_50_PERC
#define KM_TIMER4_TEST_PHASE_135_DEG KM_TIMER4_TEST_DUTY_75_PERC

// "private" variables
static const uint16_t swipeTestValues[] = { 0x0000, 0x0100, 0x1000, 0x2000, 0x4000, 0x8000, 0xc000, 0xFA00, 0xFF00, 0xFE00, 0x80 };

// "private" functions - declarations
uint16_t getSwipeTestRange(void);
void callbackOVF(void *userData);
void callbackOVFToggle(void *userData);
void callbackCompAToggle(void *userData);
void callbackCompBToggle(void *userData);
void callbackCompCToggle(void *userData);
void callbackCompAOff(void *userData);
void callbackCompBOff(void *userData);
void callbackCompCOff(void *userData);
void testSwipeCompATable(void);
void testSwipeCompA(void);
void testSwipeCompBTable(void);
void testSwipeCompB(void);
void testSwipeCompCTable(void);
void testSwipeCompC(void);
void testSwipePwm(const Tcc4PwmOut pwmOut, uint16_t maxValue);
void testSwipePwmTable(const Tcc4PwmOut pwmOut);
void appTest0(void);
void appTest1(void);
void appTest2(void);
void appTest3(void);
void appTest4(void);
void appTest5(void);
void appTest6(void);
void appTest7(void);
void appTest8(void);
void appInitDebug(void);

// "private" functions - definitions
uint16_t getSwipeTestRange(void) {
	return sizeof(swipeTestValues) / sizeof(swipeTestValues[0]);
}

void callbackOVF(void *userData) {
	//	uint16_t a = (int16_t)userData;
	dbToggle(DB_PIN_0);
	dbOn(DB_PIN_1);
	dbOn(DB_PIN_2);
	dbOn(DB_PIN_3);
}

void callbackOVFToggle(void *userData) {
	//	uint16_t a = (int16_t)userData;
	dbToggle(DB_PIN_0);
}

void callbackCompAToggle(void *userData) {
	//	int16_t a = (int16_t)userData;
	dbToggle(DB_PIN_1);
}

void callbackCompBToggle(void *userData) {
//	int16_t a = (int16_t)userData;
	dbToggle(DB_PIN_2);
}

void callbackCompCToggle(void *userData) {
//	int16_t a = (int16_t)userData;
	dbToggle(DB_PIN_3);
}

void callbackCompAOff(void *userData) {
//	int16_t a = (int16_t)userData;
	dbOff(DB_PIN_1);
}

void callbackCompBOff(void *userData) {
//	int16_t a = (int16_t)userData;
	dbOff(DB_PIN_2);
}

void callbackCompCOff(void *userData) {
//	int16_t a = (int16_t)userData;
	dbOff(DB_PIN_3);
}

void testSwipeCompATable(void) {
	uint16_t swipeRange = getSwipeTestRange();
	for (int i = 0; i < swipeRange; i++) {
		_delay_ms(KM_TIMER4_TEST_SWIPE_TABLE_DELAY);
		dbToggle(DB_PIN_4);
		kmTimer4SetValueCompA(swipeTestValues[i]);
	}
}

void testSwipeCompA(void) {
	for (uint16_t i = 0; i < KM_TIMER4_MAX - KM_TIMER4_TEST_SWIPE_ACCURACY; i += KM_TIMER4_TEST_SWIPE_ACCURACY) {
		_delay_ms(KM_TIMER4_TEST_SWIPE_DELAY);
		dbToggle(DB_PIN_4);
		kmTimer4SetValueCompA(i);
	}
}

void testSwipeCompBTable(void) {
	uint16_t swipeRange = getSwipeTestRange();
	for (uint16_t i = 0; i < swipeRange; i++) {
		_delay_ms(KM_TIMER4_TEST_SWIPE_TABLE_DELAY);
		dbToggle(DB_PIN_4);
		kmTimer4SetValueCompB(swipeTestValues[i]);
	}
}

void testSwipeCompB(void) {
	for (uint16_t i = 0; i < KM_TIMER4_MAX - KM_TIMER4_TEST_SWIPE_ACCURACY; i += KM_TIMER4_TEST_SWIPE_ACCURACY) {
		_delay_ms(KM_TIMER4_TEST_SWIPE_DELAY);
		dbToggle(DB_PIN_4);
		kmTimer4SetValueCompB(i);
	}
}

#ifdef COM4C0
void testSwipeCompCTable(void) {
	uint16_t swipeRange = getSwipeTestRange();
	for (uint16_t i = 0; i < swipeRange; i++) {
		_delay_ms(KM_TIMER4_TEST_SWIPE_TABLE_DELAY);
		dbToggle(DB_PIN_4);
		kmTimer4SetValueCompC(swipeTestValues[i]);
	}
}

void testSwipeCompC(void) {
	for (uint16_t i = 0; i < KM_TIMER4_MAX - KM_TIMER4_TEST_SWIPE_ACCURACY; i += KM_TIMER4_TEST_SWIPE_ACCURACY) {
		_delay_ms(KM_TIMER4_TEST_SWIPE_DELAY);
		dbToggle(DB_PIN_4);
		kmTimer4SetValueCompC(i);
	}
}
#endif /* COM4C0 */

void testSwipePwm(const Tcc4PwmOut pwmOut, uint16_t maxValue) {
	kmTimer4SetPwmInversion(pwmOut, false);
	for (uint16_t i = 0; i <= maxValue - KM_TIMER4_TEST_SWIPE_ACCURACY; i+=KM_TIMER4_TEST_SWIPE_ACCURACY) {
		kmTimer4SetPwmDutyBottomToTop(pwmOut, i);
		dbToggle(DB_PIN_4);
		_delay_ms(KM_TIMER4_TEST_SWIPE_DELAY);
	}

	kmTimer4SetPwmInversion(pwmOut, true);
	for (uint16_t i = maxValue; i >= KM_TIMER4_TEST_SWIPE_ACCURACY; i-=KM_TIMER4_TEST_SWIPE_ACCURACY) {
		kmTimer4SetPwmDutyBottomToTop(pwmOut, i);
		dbToggle(DB_PIN_4);
		_delay_ms(KM_TIMER4_TEST_SWIPE_DELAY);
	}
}

void testSwipePwmTable(const Tcc4PwmOut pwmOut) {
	uint16_t swipeRange = getSwipeTestRange();

	kmTimer4SetPwmInversion(pwmOut, false);
	for (int i = 0; i < swipeRange; i++) {
		_delay_ms(KM_TIMER4_TEST_SWIPE_TABLE_DELAY);
		dbToggle(DB_PIN_4);
		kmTimer4SetPwmDutyBottomToTop(pwmOut, swipeTestValues[i]);
	}
	for (int i = 0; i < 10; i++) {
		_delay_ms(10);
		dbToggle(DB_PIN_4);
	}
	kmTimer4SetPwmInversion(pwmOut, true);
	for (int i = 0; i < swipeRange; i++) {
		_delay_ms(KM_TIMER4_TEST_SWIPE_TABLE_DELAY);
		dbToggle(DB_PIN_4);
		kmTimer4SetPwmDutyBottomToTop(pwmOut, swipeTestValues[i]);
	}
}

void appTest0(void) {
	kmTimer4InitOnAccuratePeriodGenerateOutputClockA(KM_TIMER4_TEST_1MS);
	kmTimer4Start();
}

void appTest1(void) {
	kmTimer4InitOnAccurateTimeCompAInterruptCallback(KM_TIMER4_TEST_5MS, true);

#ifdef KM_TIMER4_TEST_INTERRUPTS
	kmTimer4RegisterCallbackCompA(KM_TIMER4_USER_DATA(KM_TIMER4_TEST_USER_DATA_A), callbackCompAToggle);
	kmTimer4EnableInterruptCompA();
#endif /* KM_TIMER4_TEST_INTERRUPTS */

	kmTimer4Start();
}

void appTest2(void) {
	kmTimer4InitOnPrescalerBottomToTopOvfCompABCInterruptCallback(KM_TCC0_PRSC_8);

#ifdef KM_TIMER4_TEST_INTERRUPTS
	kmTimer4RegisterCallbackOVF(KM_TIMER4_USER_DATA(KM_TIMER4_TEST_USER_DATA_D), callbackOVFToggle);
	kmTimer4EnableInterruptOVF();

	kmTimer4RegisterCallbackCompA(KM_TIMER4_USER_DATA(KM_TIMER4_TEST_USER_DATA_A), callbackCompAToggle);
	kmTimer4SetValueCompA(KM_TIMER4_TEST_DUTY_25_PERC);
	kmTimer4EnableInterruptCompA();

#ifdef OCR4B
	kmTimer4RegisterCallbackCompB(KM_TIMER4_USER_DATA(KM_TIMER4_TEST_USER_DATA_B), callbackCompBToggle);
	kmTimer4SetValueCompB(KM_TIMER4_TEST_DUTY_50_PERC);
	kmTimer4EnableInterruptCompB();
#endif /* OCR4B */
#ifdef COM4C0
	kmTimer4RegisterCallbackCompC(KM_TIMER4_USER_DATA(KM_TIMER4_TEST_USER_DATA_C), callbackCompCToggle);
	kmTimer4SetValueCompC(KM_TIMER4_TEST_DUTY_75_PERC);
	kmTimer4EnableInterruptCompC();
#endif /* COM4C0 */
#endif /* KM_TIMER4_TEST_INTERRUPTS */

	kmTimer4ConfigureOCA(KM_TCC4_A_COMP_OUT_TOGGLE);
#ifdef OCR4B
	kmTimer4ConfigureOCB(KM_TCC4_B_COMP_OUT_TOGGLE);
#endif /* OCR4B */
#ifdef COM4C0
	kmTimer4ConfigureOCC(KM_TCC4_C_COMP_OUT_TOGGLE);
#endif /* COM4C0 */

	kmTimer4Start();

#ifdef KM_TIMER4_TEST_SWIPE
	testSwipeCompA();
	kmTimer4SetValueCompA(KM_TIMER4_MID);
#ifdef OCR4B
	testSwipeCompB();
	kmTimer4SetValueCompB(KM_TIMER4_MID);
#endif /* OCR4B */
#ifdef COM4C0
	testSwipeCompC();
	kmTimer4SetValueCompC(KM_TIMER4_MID);
#endif /* COM4C0 */
#endif /* KM_TIMER4_TEST_SWIPE */

}

void appTest3(void) {
	kmTimer4InitOnAccurateTimeCompABInterruptCallback(KM_TIMER4_TEST_1MS, KM_TIMER4_TEST_PHASE_45_DEG); // for 1 millisecond

#ifdef KM_TIMER4_TEST_INTERRUPTS
	kmTimer4RegisterCallbackCompA(KM_TIMER4_USER_DATA(KM_TIMER4_TEST_USER_DATA_A), callbackCompAToggle);
	kmTimer4EnableInterruptCompA();

#ifdef OCR4B
	kmTimer4RegisterCallbackCompB(KM_TIMER4_USER_DATA(KM_TIMER4_TEST_USER_DATA_B), callbackCompBToggle);
	kmTimer4EnableInterruptCompB();
#endif /* OCR4B */

#endif /* KM_TIMER4_TEST_INTERRUPTS */
	kmTimer4Start();
}

void appTest4(void) {
#ifdef COM4C0
	kmTimer4InitOnPrescalerBottomToTopFastPwm(KM_TCC4_PRSC_1, KM_TIMER4_TEST_DUTY_25_PERC, false, KM_TIMER4_TEST_DUTY_50_PERC, false, KM_TIMER4_TEST_DUTY_75_PERC, false);
#else /* COM4C0 */
	kmTimer4InitOnPrescalerBottomToTopFastPwm(KM_TCC4_PRSC_1, KM_TIMER4_TEST_DUTY_25_PERC, false, KM_TIMER4_TEST_DUTY_50_PERC, false);
#endif /* COM4C0 */


#ifdef KM_TIMER4_TEST_INTERRUPTS
	kmTimer4RegisterCallbackOVF(KM_TIMER4_USER_DATA(KM_TIMER4_TEST_USER_DATA_D), callbackOVFToggle);
	kmTimer4EnableInterruptOVF();

	kmTimer4RegisterCallbackCompA(KM_TIMER4_USER_DATA(KM_TIMER4_TEST_USER_DATA_A), callbackCompAToggle);
	kmTimer4EnableInterruptCompA();
#ifdef OCR4B
	kmTimer4RegisterCallbackCompB(KM_TIMER4_USER_DATA(KM_TIMER4_TEST_USER_DATA_B), callbackCompBToggle);
	kmTimer4EnableInterruptCompB();
#endif /* OCR4B */

#ifdef COM4C0
	kmTimer4RegisterCallbackCompC(KM_TIMER4_USER_DATA(KM_TIMER4_TEST_USER_DATA_C), callbackCompCToggle);
	kmTimer4EnableInterruptCompC();
#endif /* COM4C0 */
#endif /* KM_TIMER4_TEST_INTERRUPTS */

	kmTimer4Start();

#ifdef KM_TIMER4_TEST_SWIPE
#ifdef OCR4B
	testSwipePwm(KM_TCC4_PWM_OUT_B, KM_TIMER4_MAX);
#endif /* OCR4B */
	testSwipePwm(KM_TCC4_PWM_OUT_A, KM_TIMER4_MAX);
#endif /* KM_TIMER4_TEST_SWIPE */
}

void appTest5(void) {
#ifdef COM4C0
	uint16_t cyclesRange = kmTimer4InitOnAccurateTimeFastPwm(KM_TIMER4_TEST_1MS, KM_TIMER4_TEST_DUTY_25_PERC, false, KM_TIMER4_TEST_DUTY_50_PERC, false, KM_TIMER4_TEST_DUTY_25_PERC, false);
#else /* COM4C0 */
	uint16_t cyclesRange = kmTimer4InitOnAccurateTimeFastPwm(KM_TIMER4_TEST_1MS, KM_TIMER4_TEST_DUTY_25_PERC, false, KM_TIMER4_TEST_DUTY_50_PERC, false);
#endif /* COM4C0 */

#ifdef KM_TIMER4_TEST_INTERRUPTS
	kmTimer4RegisterCallbackOVF(KM_TIMER4_USER_DATA(KM_TIMER4_TEST_USER_DATA_D), callbackOVFToggle);
	kmTimer4EnableInterruptOVF();

	kmTimer4RegisterCallbackCompA(KM_TIMER4_USER_DATA(KM_TIMER4_TEST_USER_DATA_A), callbackCompAToggle);
	kmTimer4EnableInterruptCompA();

#ifdef OCR4B
	kmTimer4RegisterCallbackCompB(KM_TIMER4_USER_DATA(KM_TIMER4_TEST_USER_DATA_B), callbackCompBToggle);
	kmTimer4EnableInterruptCompB();
#endif /* OCR4B */

#ifdef COM4C0
	kmTimer4RegisterCallbackCompC(KM_TIMER4_USER_DATA(KM_TIMER4_TEST_USER_DATA_C), callbackCompCToggle);
	kmTimer4EnableInterruptCompC();
#endif /* COM4C0 */

#endif /* KM_TIMER4_TEST_INTERRUPTS */
	kmTimer4Start();

#ifdef KM_TIMER4_TEST_SWIPE
#ifdef COM4C0
	testSwipePwm(KM_TCC4_PWM_OUT_C, cyclesRange);
	kmTimer4SetValueCompC(cyclesRange >> KMC_DIV_BY_2);
#endif /* COM4C0 */
#ifdef OCR4B
	testSwipePwm(KM_TCC4_PWM_OUT_B, cyclesRange);
	kmTimer4SetValueCompB(cyclesRange >> KMC_DIV_BY_2);
#endif /* OCR4B */
	testSwipePwm(KM_TCC4_PWM_OUT_A, cyclesRange);
	kmTimer4SetValueCompA(cyclesRange >> KMC_DIV_BY_2);
#endif /* KM_TIMER4_TEST_SWIPE */
}

void appTest6(void) {
#ifdef COM4C0
	kmTimer4InitOnPrescalerBottomToTopPcPwm(KM_TCC0_PRSC_1, KM_TIMER4_TEST_DUTY_25_PERC, false, KM_TIMER4_TEST_DUTY_50_PERC, false, KM_TIMER4_TEST_DUTY_75_PERC, false);
#else /* COM4C0 */
	kmTimer4InitOnPrescalerBottomToTopPcPwm(KM_TCC0_PRSC_1, KM_TIMER4_TEST_DUTY_25_PERC, false, KM_TIMER4_TEST_DUTY_50_PERC, false);
#endif /* COM4C0 */

#ifdef KM_TIMER4_TEST_INTERRUPTS
	kmTimer4RegisterCallbackOVF(KM_TIMER4_USER_DATA(KM_TIMER4_TEST_USER_DATA_D), callbackOVF);
	kmTimer4EnableInterruptOVF();

	kmTimer4RegisterCallbackCompA(KM_TIMER4_USER_DATA(KM_TIMER4_TEST_USER_DATA_A), callbackCompAOff);
	kmTimer4EnableInterruptCompA();
#ifdef OCR4B
	kmTimer4RegisterCallbackCompB(KM_TIMER4_USER_DATA(KM_TIMER4_TEST_USER_DATA_B), callbackCompBOff);
	kmTimer4EnableInterruptCompB();
#endif /* OCR4B */

#ifdef COM4C0
	kmTimer4RegisterCallbackCompC(KM_TIMER4_USER_DATA(KM_TIMER4_TEST_USER_DATA_C), callbackCompCOff);
	kmTimer4EnableInterruptCompC();
#endif /* COM4C0 */
#endif /* KM_TIMER4_TEST_INTERRUPTS */
	kmTimer4Start();

#ifdef KM_TIMER4_TEST_SWIPE
	testSwipePwm(KM_TCC4_PWM_OUT_A, KM_TIMER4_MAX);
	kmTimer4SetValueCompA(KM_TIMER4_MID);
#ifdef OCR4B
	testSwipePwm(KM_TCC4_PWM_OUT_B, KM_TIMER4_MAX);
	kmTimer4SetValueCompB(KM_TIMER4_MID);
#endif /* OCR4B */
#ifdef COM4C0
	testSwipePwm(KM_TCC4_PWM_OUT_C, KM_TIMER4_MAX);
	kmTimer4SetValueCompC(KM_TIMER4_MID);
#endif /* COM4C0 */
#endif /* KM_TIMER4_TEST_SWIPE */
}

void appTest7(void) {
#ifdef COM4C0
	uint16_t cyclesRange = kmTimer4InitOnAccurateTimePcPwm(KM_TIMER4_TEST_1MS, KM_TIMER4_TEST_DUTY_25_PERC, false, KM_TIMER4_TEST_DUTY_50_PERC, false, KM_TIMER4_TEST_DUTY_75_PERC, false);
#else /* COM4C0 */
	uint16_t cyclesRange = kmTimer4InitOnAccurateTimePcPwm(KM_TIMER4_TEST_1MS, KM_TIMER4_TEST_DUTY_25_PERC, false, KM_TIMER4_TEST_DUTY_50_PERC, false);
#endif /* COM4C0 */

#ifdef KM_TIMER4_TEST_INTERRUPTS
	kmTimer4RegisterCallbackOVF(KM_TIMER4_USER_DATA(KM_TIMER4_TEST_USER_DATA_D), callbackOVF);
	kmTimer4EnableInterruptOVF();

	kmTimer4RegisterCallbackCompA(KM_TIMER4_USER_DATA(KM_TIMER4_TEST_USER_DATA_A), callbackCompAOff);
	kmTimer4EnableInterruptCompA();
#ifdef OCR4B
	kmTimer4RegisterCallbackCompB(KM_TIMER4_USER_DATA(KM_TIMER4_TEST_USER_DATA_B), callbackCompBOff);
	kmTimer4EnableInterruptCompB();
#endif /* OCR4B */

#ifdef COM4C0
	kmTimer4RegisterCallbackCompC(KM_TIMER4_USER_DATA(KM_TIMER4_TEST_USER_DATA_C), callbackCompCOff);
	kmTimer4EnableInterruptCompC();
#endif /* COM4C0 */
#endif /* KM_TIMER4_TEST_INTERRUPTS */

	kmTimer4Start();

#ifdef KM_TIMER4_TEST_SWIPE
	testSwipePwm(KM_TCC4_PWM_OUT_A, cyclesRange);
	kmTimer4SetValueCompA(cyclesRange >> KMC_DIV_BY_2);
#ifdef OCR4B
	testSwipePwm(KM_TCC4_PWM_OUT_B, cyclesRange);
	kmTimer4SetValueCompB(cyclesRange >> KMC_DIV_BY_2);
#endif /* OCR4B */
#ifdef COM4C0
	testSwipePwm(KM_TCC4_PWM_OUT_C, cyclesRange);
	kmTimer4SetValueCompC(cyclesRange >> KMC_DIV_BY_2);
#endif /* COM4C0 */
#endif /* KM_TIMER4_TEST_SWIPE */
}

void appTest8(void) {
	kmTimer4Init(KM_TCC4_PRSC_1024, KM_TCC4_MODE_5_A, KM_TCC4_MODE_5_B);

	kmTimer4SetValueCompA(4);
	kmTimer4ConfigureOCA(KM_TCC4_A_PWM_COMP_OUT_CLEAR_UP_SET_DOWN);

#ifdef OCR4B
	kmTimer4SetValueCompB(9);
	kmTimer4ConfigureOCB(KM_TCC4_B_PWM_COMP_OUT_CLEAR_UP_SET_DOWN);
#endif /* OCR4B */
#ifdef COM4C0
	kmTimer4SetValueCompC(128);
	kmTimer4ConfigureOCC(KM_TCC4_C_PWM_COMP_OUT_CLEAR_UP_SET_DOWN);
#endif /* COM4C0 */

	kmTimer4Start();
}

void appInitDebug(void) {
	dbPullUpAllPorts();
	dbInit();
	dbOff(DB_PIN_0);
	dbOff(DB_PIN_1);
	dbOff(DB_PIN_2);
	dbOff(DB_PIN_3);
	dbOff(DB_PIN_4);
}

// "public" functions
void appInit(void) {
	kmCpuDisableWatchdogAndInterruptsOnStartup();
	appInitDebug();
	kmCpuInterruptsEnable();

	static const uint16_t testNumber = KM_TIMER4_TEST_NUMBER;

	switch (testNumber) {
		case 0: {
			appTest0();
			break;
		}
		case 1: {
			appTest1();
			break;
		}
		case 2: {
			appTest2();
			break;
		}
		case 3: {
			appTest3();
			break;
		}
		case 4: {
			appTest4();
			break;
		}
		case 5: {
			appTest5();
			break;
		}
		case 6: {
			appTest6();
			break;
		}
		case 7: {
			appTest7();
			break;
		}
		case 8: {
			appTest8();
			break;
		}
	}
}

void appLoop(void) {
}
