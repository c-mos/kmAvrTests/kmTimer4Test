/** @file
 * @brief Project configuration definitions.
 * config.h
 *
 *  Created on: Jan 16, 2024
 *      Author: Krzysztof Moskwa
 *      License: GPL-3.0-or-later
 *
 *  kmTimer4Test Application for testing kmTimer4 library from AVR kmFramework
 *  Copyright (C) 2024  Krzysztof Moskwa
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


#ifndef CONFIG_H_
#define CONFIG_H_

// prototype ports
//#define KM_USE_MOMIK_2560
#define KM_USE_ASTAR_328PB_MICRO

#ifdef KM_USE_ASTAR_328PB_MICRO
// kmDebug
#define KM_DEBUG_DDR DDRC
#define KM_DEBUG_PORT PORTC
#define KM_DEBUG_PIN_0 PC0 // Overflow
#define KM_DEBUG_PIN_1 PC1 // CompA
#define KM_DEBUG_PIN_2 PC2 // CompB
#define KM_DEBUG_PIN_3 PC3 // CompC
#define KM_DEBUG_PIN_4 PC4 // Swipe trigger
#endif /* KM_USE_ASTAR_328PB_MICRO */

#ifdef KM_USE_MOMIK_2560
// kmDebug
#define KM_DEBUG_DDR DDRK
#define KM_DEBUG_PORT PORTK
#define KM_DEBUG_PIN_0 PK0 // Overflow
#define KM_DEBUG_PIN_1 PK1 // CompA 
#define KM_DEBUG_PIN_2 PK2 // CompB
#define KM_DEBUG_PIN_3 PK3 // CompC
#define KM_DEBUG_PIN_4 PK4 // Swipe trigger
#endif

#define KM_TIMER4_TEST_SWIPE
#define KM_TIMER4_TEST_SWIPE_ACCURACY 8
#define KM_TIMER4_TEST_SWIPE_DELAY 1
#define KM_TIMER4_TEST_SWIPE_TABLE_DELAY 100

#include "kmTimer4/kmTimer4DefaultConfig.h"

#define KM_TIMER4_TEST_NUMBER 0
#define KM_TIMER4_TEST_INTERRUPTS

#endif /* CONFIG_H_ */
