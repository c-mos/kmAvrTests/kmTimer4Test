/** @file
 * @brief Version file.
 * version.h
 *
 *  Created on: Jan 16, 2024
 *      Author: Krzysztof Moskwa
 *      License: GPL-3.0-or-later
 *
 *  kmTimer4Test Application for testing kmTimer4 library from AVR kmFramework
 *  Copyright (C) 2024  Krzysztof Moskwa
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

#ifndef VERSION_H_
#define VERSION_H_

#define APP_NAME	"kmTimer3Test"
#define APP_VERSION	"1.0.0"
#define APP_YEAR	"2024"
#define APP_AUTHOR	"Krzysztof Moskwa"
#define APP_REPO	"https://gitlab.com/c-mos/kmAvrTests/kmTimer3Test"

#endif /* VERSION_H_ */
